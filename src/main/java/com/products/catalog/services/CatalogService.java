package com.products.catalog.services;

import com.products.catalog.dto.AttributesDTO;

import com.products.catalog.dto.CategoryDTO;
import com.products.catalog.dto.ProductDTO;
import com.products.catalog.models.Attributes;
import com.products.catalog.models.Category;
import com.products.catalog.models.Product;
import com.products.catalog.repository.AttributesRepository;
import com.products.catalog.repository.CategoryRepository;
import com.products.catalog.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;

@Service
public class CatalogService {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    AttributesRepository attributesRepository;

    @Autowired
    ProductRepository productRepository;

    public Map<String, String> createCategory(CategoryDTO categoryDTO) {

        Map<String, String> result = new HashMap<>();
        try {
            Category category = convertCategoryDTOToCategory(categoryDTO);
            categoryRepository.save(category);
            result.put("SUCCESS", "Category added with id : " + category.getCategoryId());

        } catch (Exception e) {
            result.put("FAIL", e.getMessage());
        }
        return result;
    }

    private Category convertCategoryDTOToCategory(CategoryDTO categoryDTO) {
        Category category = new Category();
        List<Attributes> attributes = new ArrayList<>();
        category.setCategoryId(categoryDTO.getCategoryId());
        category.setCategoryName(categoryDTO.getCategoryName());
        if (categoryDTO.getAttributesList() != null && !categoryDTO.getAttributesList().isEmpty()) {
            categoryDTO.getAttributesList().stream().forEach(attributeDto -> {
                Attributes attribute = new Attributes();
                attribute.setAttributeId(attributeDto.getAttributeId());
                attributes.add(attribute);
            });
            category.setAttributes(attributes);
        }
        return category;
    }

    public Map<String, String> createCategoryAttributes(AttributesDTO attributesDTO) {

        Map<String, String> result = new HashMap<>();
        try {
            Attributes attribute = convertAttributesDTOToAttribute(attributesDTO);
            attributesRepository.save(attribute);
            result.put("SUCCESS", "Attributes added with id : " + attribute.getAttributeId());

        } catch (Exception e) {
            result.put("FAIL", e.getMessage());
        }
        return result;
    }

    private Attributes convertAttributesDTOToAttribute(AttributesDTO attributesDTO) {
        Attributes attribute = new Attributes();
        attribute.setAttributeId(attributesDTO.getAttributeId());
        attribute.setAttributeName(attributesDTO.getAttributeName());
        attribute.setAttributeValue(attributesDTO.getAttributeValue());
        return attribute;
    }

    public Object createProduct(ProductDTO productDTO) {
        Map<String, String> result = new HashMap<>();
        try {
            Product product = convertProductDTOToProduct(productDTO);
            productRepository.save(product);
            result.put("SUCCESS", "Product added with id : " + product.getProductId());

        } catch (Exception e) {
            result.put("FAIL", e.getMessage());
        }
        return result;
    }

    private Product convertProductDTOToProduct(ProductDTO productDTO) {
        Product product =new Product();
        product.setProductId(productDTO.getProductId());
        product.setProductName(productDTO.getProductName());
        product.setCategory(convertCategoryDTOToCategory(productDTO.getCategoryDTO()));
        return product;
    }

    public CategoryDTO getCategoryInfo(String categoryId) throws EntityNotFoundException {
            final Optional<Category> optionalCategory =
                    categoryRepository.findById(categoryId);
        CategoryDTO categoryDTO = null;
            if (optionalCategory.isPresent()) {
                Category category = optionalCategory.get();
                categoryDTO=convertCategoryToCategoryDTO(category);
            } else {
                throw new EntityNotFoundException("Category entity Not Found with id "+ categoryId);
            }
            return categoryDTO;
    }

    private CategoryDTO convertCategoryToCategoryDTO(Category category) {
        CategoryDTO categoryDTO=new CategoryDTO();
        categoryDTO.setCategoryId(category.getCategoryId());
        categoryDTO.setCategoryName(category.getCategoryName());
        categoryDTO.setAttributesList(convertAtrubutesToAtrributesDTO(category.getAttributes()));
        return categoryDTO;
    }

    private List<AttributesDTO> convertAtrubutesToAtrributesDTO(List<Attributes> attributes) {
        List<AttributesDTO> attributesList=new ArrayList<>();
        attributes.parallelStream().forEach(attribute -> {
            AttributesDTO attributesDTO=new AttributesDTO();
            attributesDTO.setAttributeId(attribute.getAttributeId());
            attributesDTO.setAttributeName(attribute.getAttributeName());
            attributesDTO.setAttributeValue(attribute.getAttributeValue());
            attributesList.add(attributesDTO);
        });
        return  attributesList;
    }

    public ProductDTO getProductInfo(String productId) throws EntityNotFoundException {
        final Optional<Product> optionalProduct =
                productRepository.findById(productId);
        ProductDTO productDTO = null;
        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();
            productDTO=convertProductToProductDTO(product);
        } else {
            throw new EntityNotFoundException("Product entity Not Found with id "+ productId);
        }
        return productDTO;
    }

    private ProductDTO convertProductToProductDTO(Product product) {
        ProductDTO productDTO=new ProductDTO();
        productDTO.setProductId(product.getProductId());
        productDTO.setProductName(product.getProductName());
        productDTO.setCategoryDTO(convertCategoryToCategoryDTO(product.getCategory()));
        return productDTO;
    }
}
