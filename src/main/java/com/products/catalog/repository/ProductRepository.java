package com.products.catalog.repository;

import com.products.catalog.models.Category;
import com.products.catalog.models.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, String> {

}
