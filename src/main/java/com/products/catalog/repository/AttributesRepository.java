package com.products.catalog.repository;

import com.products.catalog.models.Attributes;
import com.products.catalog.models.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttributesRepository extends CrudRepository<Attributes, String> {

}
