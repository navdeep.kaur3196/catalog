package com.products.catalog.dto;

import com.products.catalog.models.Attributes;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.util.List;

public class CategoryDTO {

    private String categoryId;
    private String categoryName;
    private List<AttributesDTO> attributesList;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<AttributesDTO> getAttributesList() {
        return attributesList;
    }

    public void setAttributesList(List<AttributesDTO> attributesList) {
        this.attributesList = attributesList;
    }

}
