package com.products.catalog.controller;

import com.products.catalog.dto.AttributesDTO;
import com.products.catalog.dto.CategoryDTO;
import com.products.catalog.dto.ProductDTO;
import com.products.catalog.models.Product;
import com.products.catalog.services.CatalogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@RestController
public class CatalogController {

    private static final Logger logger = LoggerFactory.getLogger(CatalogController.class);

    @Autowired
    private CatalogService catalogService;

    @PostMapping("/v1/category")
    public ResponseEntity<Object> createCategory(@RequestBody CategoryDTO categoryDTO) {
        logger.debug("Method:: createCategory");
        return new ResponseEntity<>(catalogService.createCategory(categoryDTO), HttpStatus.OK);
    }

    @PostMapping("/v1/category/attributes")
    public ResponseEntity<Object> createCategoryAttributes(@RequestBody AttributesDTO attributesDTO) {
        logger.debug("Method:: createCategoryAttributes");
        return new ResponseEntity<>(catalogService.createCategoryAttributes(attributesDTO), HttpStatus.OK);
    }

    @PostMapping("/v1/product")
    public ResponseEntity<Object> createProduct(@RequestBody ProductDTO productDTO) {
        logger.debug("Method:: createProduct");
        return new ResponseEntity<>(catalogService.createProduct(productDTO), HttpStatus.OK);
    }

    @GetMapping("/v1/category/{categoryId")
    public CategoryDTO getCategoryInfo(@PathVariable("categoryId") final String categoryId)  throws EntityNotFoundException {
        logger.debug("Method:: getCategoryInfo");
        return catalogService.getCategoryInfo(categoryId);
    }

    @GetMapping("/v1/category/{categoryId")
    public ProductDTO getProductInfo(@PathVariable("productId") final String productId)  throws EntityNotFoundException {
        logger.debug("Method:: getProductInfo");
        return catalogService.getProductInfo(productId);
    }

}
